package utilities;

public class MatrixMethod {
	public static int[][] duplicate(int[][] x){
		int[][]newX=new int [x.length][x[0].length*2];
		
		for(int i=0; i<x.length; i++){
			
			for(int j=0; j<x[i].length; j++) {
				newX [i][j*2]=x[i][j];
				newX [i][j*2+1]=x[i][j];
			}
		}
		return newX;
	}

}
