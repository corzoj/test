package tests;
import automobiles.Car;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	void CarConstructortest() {
		Car car= new Car(10);
		assertEquals(10,car.getSpeed());
		
	    try {
	        	Car c = new Car(-10); 
	            fail("Negative speed");  
	        }
	    		catch (IllegalArgumentException e) {         
	        }
	        	catch (Exception e) {
	        		fail("negative speed!");
	        	}

	}
	
	@Test
	void getSpeedTest() {
		Car c= new Car(10);
		assertEquals(10,c.getSpeed());
		
	}
	
	@Test
	void getLocationTest() {
		Car c= new Car (10);
		assertEquals(50,c.getLocation());
	}
	@Test
	void moveRigthTest() {
		Car c=new Car(10);
		c.moveRight();
		assertEquals(60,c.getLocation());
	} 
	@Test
	void moveLeftTest() {
		Car c=new Car(10);
		c.moveLeft();
		assertEquals(40,c.getLocation());
	}
	@Test
	void accelerateTest(){
		Car c=new Car(10);
		c.accelerate();
		assertEquals(11,c.getSpeed());
	}
	@Test
	public void stopTest() {
		Car c=new Car(10);
		c.stop();
		assertEquals(0,c.getSpeed());
	}
	
}
