package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTest {

	@Test
	void MatrixTest() {
		int[][]x={{1,2,3},{1,2,3}};
		int[][]newX={{1,1,2,2,3,3},{1,1,2,2,3,3}};
		assertArrayEquals(newX,MatrixMethod.duplicate(x));
	}

}
